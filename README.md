# OpenML dataset: NYC-Uber-Pickups-with-Weather-and-Holidays

https://www.openml.org/d/43746

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This is a forked subset of the Uber Pickups in New York City, enriched with weather, borough, and holidays information.

Content
I created this dataset for a personal project of exploring and predicting pickups in the area by merging data that intuitively seem possible factors for the analysis. These are:  

Uber Pickups in New York City, from 01/01/2015 to 30/06/2015 (uber-raw-data-janjune-15.csv). (by FiveThirtyEight via kaggle.com)  
Weather data from National Centers for Environmental Information.  
LocationID to Borough mapping. (by FiveThirtyEight)  
NYC public holidays.   

The main dataset contained over 10 million observations of 4 variables which aggregated per hour and borough, and then joined with the rest of the datasets producing 29,101 observations across 13 variables. These are:  

pickup_dt: Time period of the observations. 
borough: NYC's borough.
pickups: Number of pickups for the period.
spd: Wind speed in miles/hour.
vsb: Visibility in Miles to nearest tenth.
temp: temperature in Fahrenheit.
dewp: Dew point in Fahrenheit.
slp: Sea level pressure.
pcp01: 1-hour liquid precipitation.
pcp06: 6-hour liquid precipitation.
pcp24: 24-hour liquid precipitation.
sd: Snow depth in inches.
hday: Being a holiday (Y) or not (N).


Acknowledgements / Original datasets:

Uber Pickups in New York City, from 01/01/2015 to 30/06/2015 (uber-raw-data-janjune-15.csv). (by FiveThirtyEight via kaggle.com)  
Weather data from National Centers for Environmental Information.  
LocationID to Borough mapping. (by FiveThirtyEight)  

(Picture credits: Buck Ennis)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43746) of an [OpenML dataset](https://www.openml.org/d/43746). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43746/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43746/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43746/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

